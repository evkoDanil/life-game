package com.example.life_game.service;

import com.example.life_game.exception.OutOfRangeException;
import com.example.life_game.game.CellsCoords;
import com.example.life_game.game.LifeGame;
import com.example.life_game.repository.CellCoordsRepository;
import com.example.life_game.repository.CellRepository;
import com.example.life_game.repository.LifeGameRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class LifeGameService {
    private LifeGame lg;
    @Autowired
    private LifeGameRepository lifeGameRepository;
    @Autowired
    private CellRepository cellRepository;
    @Autowired
    private CellCoordsRepository cellCoordsRepository;

    public void initGame(int dimension, List<CellsCoords> liveCellsCoords) throws OutOfRangeException {
        AtomicBoolean isOutOfRange = new AtomicBoolean(false);
        liveCellsCoords.forEach(lc -> {
            if (lc.getX() >= dimension || lc.getY() >= dimension || lc.getX() < 0 || lc.getY() < 0) {
                isOutOfRange.set(true);
            }
        });
        if (!isOutOfRange.get()) {
            lg = new LifeGame();
            lg.init(dimension, liveCellsCoords);
        } else throw new OutOfRangeException("coords out of range");
    }

    public String getMenu() {
        if (!lg.isPause()) {
            lg.setPause(true);
            lg.getCells().forEach(cell -> {
                cellCoordsRepository.save(cell.getCellsCoords());
                cellRepository.save(cell);
            });
            lifeGameRepository.save(lg);
            return "Игра " + lg.getId() + " находится в состоянии паузы\n" +
                    "Выберите действие:\n" +
                    "1: Возобновить игру\n" +
                    "2: Сохранить и выйти\n" +
                    "3: Выгрузить состояние игры\n";
        } else return "Игра уже в паузе, либо не начата";
    }

    public String choseVariant(Integer variant) {
        if (variant != null) {
            if (lg.isPause()) {
                switch (variant) {
                    case 1: {
                        lg.setPause(false);
                        lg.start();
                        return "Игра " + lg.getId() + " возобновлена!";
                    }
                    case 2: {
                        return "Игра " + lg.getId() + " сохранена и прекращена!";
                    }
                    case 3: {
                        return lg.toString();
                    }
                    default: {
                        return "Выбран неверное дейтвие!";
                    }
                }
            } else return "Чтобы выбрать вариант действия переведите игру в режим паузы!";
        }
        return "variant is null";
    }

    public String downloadGame(Long gameId) {
        if (gameId != null) {
            if (!lg.isPause()) {
                return "Для начала поставьте предыдущую игру в состояние паузы!";
            }
            LifeGame newGame = lifeGameRepository.getById(gameId);
            lg.setCells(newGame.getCells());
            lg.setDimension(newGame.getDimension());
            lg.setPause(false);
            lg.setId(newGame.getId());
            lg.start();
            return "Загружена игра " + lg.getId();
        }
        return "gameId is null";
    }
}
