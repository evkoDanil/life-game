package com.example.life_game.controller;

import com.example.life_game.game.CellsCoords;
import com.example.life_game.exception.OutOfRangeException;
import com.example.life_game.service.LifeGameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/game")
@RequiredArgsConstructor
@Slf4j
public class GameController {
    private final LifeGameService lifeGameService;

    @PostMapping("init/{dimension}")
    public void initGame(@PathVariable int dimension,
                         @RequestBody List<CellsCoords> cellsCoordsList) {
        try {
            lifeGameService.initGame(dimension, cellsCoordsList);
        } catch (OutOfRangeException e) {
            log.error(e.getMessage());
        }
    }

    @GetMapping("menu")
    public String menu() {
        return lifeGameService.getMenu();
    }

    @PostMapping("/chose/{variant}")
    public String choseVariant(@PathVariable Integer variant) {
        return lifeGameService.choseVariant(variant);
    }

    @PostMapping("/download/{gameId}")
    public String downloadGame(@PathVariable Long gameId) {
        return lifeGameService.downloadGame(gameId);
    }
}
