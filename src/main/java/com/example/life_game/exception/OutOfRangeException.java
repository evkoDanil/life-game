package com.example.life_game.exception;

public class OutOfRangeException extends Exception{
    public OutOfRangeException(String message){
        super(message);
    }
}
