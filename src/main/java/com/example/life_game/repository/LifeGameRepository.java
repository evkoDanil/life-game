package com.example.life_game.repository;

import com.example.life_game.game.LifeGame;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LifeGameRepository extends JpaRepository<LifeGame, Long> {
}
