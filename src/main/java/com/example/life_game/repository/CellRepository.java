package com.example.life_game.repository;

import com.example.life_game.game.Cell;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CellRepository extends JpaRepository<Cell,Long> {
}
