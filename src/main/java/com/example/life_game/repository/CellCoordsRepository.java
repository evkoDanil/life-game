package com.example.life_game.repository;

import com.example.life_game.game.CellsCoords;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CellCoordsRepository extends JpaRepository<CellsCoords, Long> {
}
