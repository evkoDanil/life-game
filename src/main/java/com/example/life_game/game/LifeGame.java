package com.example.life_game.game;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@Builder
@Entity(name = "life_game")
public class LifeGame {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int dimension;
    private boolean isPause = true;

    @OneToMany
    @JoinColumn(name = "game_id")
    private List<Cell> cells;


    public void init(int dimension, List<CellsCoords> liveCellsCoords) {
        this.dimension = dimension;
        this.cells = new ArrayList<>();
        this.isPause = false;
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                Cell c = new Cell();
                CellsCoords coords = new CellsCoords();
                coords.setX(i);
                coords.setY(j);
                c.setCellsCoords(coords);
                cells.add(c);
            }
        }
        List<Cell> liveCells = cells.stream()
                .filter(cell -> liveCellsCoords.stream()
                        .anyMatch(lc -> lc.equals(cell.getCellsCoords())))
                .collect(Collectors.toList());

        cells.forEach(cell -> {
            if (liveCells.stream().anyMatch(lc -> lc.equals(cell))) {
                cell.setState(State.LIVE);
            }
        });
        if (!cells.isEmpty()) {
            start();
        }
    }

    public void start() {
        boolean isChangedState = true;
        while (!isPause && isChangedState) {
            printState();
            isChangedState = false;
            for (int i = 0; i < cells.size(); i++) {
                Cell currentCell = cells.get(i);
                long neighboursQuantity = 0;
                for (Cell cell : cells) {
                    if (cell.getState() == State.LIVE &&
                            !cell.getCellsCoords().equals(currentCell.getCellsCoords()) &&
                            isNeighbours(currentCell, cell)) {
                        neighboursQuantity++;
                    }

                }
                if (currentCell.getState() == State.DEAD && neighboursQuantity == 3) {
                    currentCell.setState(State.LIVE);
                    isChangedState = true;
                } else if (currentCell.getState() == State.LIVE && neighboursQuantity < 2 || neighboursQuantity > 3) {
                    currentCell.setState(State.DEAD);
                    isChangedState = true;
                }
                System.out.println("current cell " + currentCell + " neighbours quantity " + neighboursQuantity);
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }
        }
        if (!isChangedState) {
            System.out.println("Игра окончена!\n" +
                    "Состояние не изменялось!");
        }
    }

    private boolean isNeighbours(Cell currentCell, Cell neighbourCell) {
        int currentX = currentCell.getCellsCoords().getX();
        int currentY = currentCell.getCellsCoords().getY();
        int neighbourX = neighbourCell.getCellsCoords().getX();
        int neighbourY = neighbourCell.getCellsCoords().getY();

        if (neighbourCell.getState() == State.LIVE) {
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    int X = currentX - i;
                    int Y = currentY - j;
                    if (X == neighbourX && Y == neighbourY) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void printState() {
        char[][] matrix = new char[dimension][dimension];
        cells.forEach(cell -> {
            if (cell.getState() == State.LIVE) {
                matrix[cell.getCellsCoords().getX()][cell.getCellsCoords().getY()] = '*';
            } else matrix[cell.getCellsCoords().getX()][cell.getCellsCoords().getY()] = '0';
        });
        for (int i = 0; i < dimension; i++) {
            System.out.println();
            for (int j = 0; j < dimension; j++) {
                System.out.print(matrix[i][j] + " ");
            }
        }
        System.out.println();
    }
}
